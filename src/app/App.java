package app;

import model.Solid;
import model.Vertex;
import render.RasterizerTriangle;
import render.Renderer;
import render.Shader;
import model.Arrow;
import transforms.Col;

import java.util.ArrayList;
import java.util.List;

public class App {

    public void draw(){
        List<Solid> solids = new ArrayList<>();
        solids.add(new Arrow());

        Shader<Vertex,Col> shader = new Shader<Vertex, Col>() {
            @Override
            public Col shade(Vertex vertex) {
                return new Col(0xffffff);
            }
        };


        RasterizerTriangle rt = new RasterizerTriangle();
       // nejde rt.setShader(shader);
        rt.setShader((Vertex vertex) -> { return vertex.getColor().mul(1/vertex.getOne());});


        Renderer renderer = new Renderer(rt);

        renderer.render(solids);
    }

}
