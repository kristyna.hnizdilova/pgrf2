package model;

import transforms.Bicubic;
import transforms.Cubic;
import transforms.Point3D;

public class Surface extends Solid {

    private Bicubic bc;

    public Surface() {
        Point3D[] rb = new Point3D[16];
        rb[0]=new Point3D(0,0,0);
        rb[1]=new Point3D(0,0,0);
        rb[2]=new Point3D(0,0,0);
        rb[3]=new Point3D(0,0,0);
        rb[4]=new Point3D(0,0,0);
        rb[5]=new Point3D(0,0,0);
        rb[6]=new Point3D(0,0,0);
        rb[7]=new Point3D(0,0,0);
        rb[8]=new Point3D(0,0,0);
        rb[9]=new Point3D(0,0,0);
        rb[10]=new Point3D(0,0,0);
        rb[11]=new Point3D(0,0,0);
        rb[12]=new Point3D(0,0,0);
        rb[13]=new Point3D(0,0,0);
        rb[14]=new Point3D(0,0,0);
        rb[15]=new Point3D(0,0,0);

        bc = new Bicubic(Cubic.BEZIER, rb);

        int index = 0;
        for (int u = 0; u <= 1; u+= 0.1) {
            for (int v = 0; v<= 1; v+= 0.1) {
            Vertex ver = new Vertex(bc.compute(u,v));
            getVertexBuffer().add(ver);
            getIndexBuffer().add(index++);
            }
        }
        getParts().add(new Part(TypeTopology.POINTS,0,indexBuffer.size()));
    }
}
