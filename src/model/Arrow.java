package model;

import model.Part;
import model.Solid;
import model.TypeTopology;
import model.Vertex;
import transforms.Point3D;

import java.util.ArrayList;
import java.util.List;

public class Arrow extends Solid {

    public Arrow(){
        super();
        vertexBuffer.add(new Vertex(new Point3D(0,0,0)));
        vertexBuffer.add(new Vertex(new Point3D(0,0,0)));
        vertexBuffer.add(new Vertex(new Point3D(0,0,0)));
        vertexBuffer.add(new Vertex(new Point3D(0,0,0)));

        indexBuffer.add(0);
        indexBuffer.add(1);
        indexBuffer.add(2);
        indexBuffer.add(1);
        indexBuffer.add(3);

        parts.add(new Part(TypeTopology.LINES,3,1));
        parts.add(new Part(TypeTopology.TRIANGLES,0,1));
    }


}
