package model;

import transforms.Col;
import transforms.Point3D;

public class Vertex {

    private final Point3D point;
    private Col color;



    private double one = 1;

    public Vertex(Point3D point) {
        this.point = point;
        color = new Col(Math.abs(point.getX()), Math.abs(point.getY()), Math.abs(point.getZ()));
        one = 1.0;
    }

    public Vertex() {
        point = new Point3D();
        color = new Col();
        one = 1.0;
    }

    public Vertex(double x, double y, double z) {
        point = new Point3D(x, y, z);
        color = new Col(Math.abs(x), Math.abs(y), Math.abs(z));
        one = 1.0;
    }

    public Vertex(Point3D point, Col color) {
        this.point = new Point3D(point);
        this.color = color;
        one = 1.0;
    }

    public double getOne() {
        return 1.0;
    }

    public Point3D getPoint(){
        return point;
    }

    public Vertex withPoint(Point3D point){
        return new Vertex(point);
    }

    public Vertex mul(double d){
        return new Vertex(point.mul(d));
    }

    public Vertex add(Vertex v){
        return new Vertex(point.add(v.getPoint()));
    }

    public Vertex dehomog(){
        return this.mul(1/this.getPoint().getW());
    }

    public Col getColor() {
        return color;
    }
}
