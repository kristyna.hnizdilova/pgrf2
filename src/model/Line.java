package model;

import transforms.Point3D;

public class Line extends  Solid{

    public Line() {

        getVertexBuffer().add(new Vertex(new Point3D(0,0,0)));
        getVertexBuffer().add(new Vertex(new Point3D(1,1,1)));

        getIndexBuffer().add(0);
        getIndexBuffer().add(1);

        new Part(TypeTopology.LINES,0,1);
    }

}
