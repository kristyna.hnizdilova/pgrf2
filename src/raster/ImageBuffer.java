package raster;

import transforms.Col;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageBuffer implements Raster<Col>{


    private BufferedImage img;

    public ImageBuffer(BufferedImage img){
        this.img = img;
    }


    @Override
    public Col get(int x, int y) {
        return new Col(img.getRGB(x,y));
    }

    @Override
    public void set(int x, int y, Col value) {
        img.setRGB(x, y, value.getRGB());
    }

    @Override
    public int getWidth() {
        return img.getWidth();
    }

    @Override
    public int getHeight() {
        return img.getHeight();
    }

    @Override
    public void clear(Col col) {
        Graphics g = img.getGraphics();
        g.fillRect(0,0,img.getWidth(),img.getHeight());
    }


}
