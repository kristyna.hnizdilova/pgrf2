package raster;

import transforms.Col;

import java.awt.*;
import java.awt.image.BufferedImage;

public class TestVisibility {

    private ImageBuffer imageBuffer;
    private ZBuffer<Float> zBuffer;




    public TestVisibility(BufferedImage img){
        this.imageBuffer = new ImageBuffer(img);
        this.zBuffer = new ZBuffer<Float>(img.getWidth(),img.getHeight());

    }



    public void render(int x, int y, double z, Col color){

        if(zBuffer.get(x, y)>z){
            zBuffer.set(x,y,(float)z);
            imageBuffer.set(x,y,color);
        }

    }

    public void clear(Col col){
        imageBuffer.clear(col);
        zBuffer.clear((float)1.0);
    }

    public double getWidth(){
        return imageBuffer.getWidth();
    }

    public double getHeight(){
        return imageBuffer.getHeight();
    }





}
