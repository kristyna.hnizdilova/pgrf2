package raster;

import java.util.ArrayList;
import java.util.List;

public class ZBuffer<T> implements Raster<T> {


    private int width;
    private int height;
    private List<T> zBuffer;


    public ZBuffer(int width, int height){
        this.width = width;
        this.height = height;
        this.zBuffer = new ArrayList<>(width * height);
    }


    @Override
    public T get(int x, int y) {
        return zBuffer.get(width*y+x);
    }

    @Override
    public void set(int x, int y, T value) {
        zBuffer.set(width * y + x, value);
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }


    //TODO ZBuffer clear
    @Override
    public void clear(T t) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {

                // zBuffer.set(width * i + j, height, );
            }
        }
    }

}
