package render;

import model.Part;
import model.Solid;
import model.Vertex;
import raster.ZBuffer;
import transforms.Mat4;
import transforms.Mat4Scale;

import java.util.List;

public class Renderer {


    private RasterizerTriangle rasterizerTrianglert;

    public Renderer(RasterizerTriangle rasterizerTriangle) {
    }



    private Mat4 model = new Mat4Scale(3	,3,3);
    private Mat4 view;
    private Mat4 projection;

    ZBuffer zb;

    public void setModel(Mat4 model) {
        this.model = model;
    }

    public void setView(Mat4 view) {
        this.view = view;
    }

    public void setProjection(Mat4 projection) {
        this.projection = projection;
    }

    private Mat4 finalMat() {
        return model.mul(view).mul(projection);
    }



    public void render(List<Solid> solids){
        for (Solid solid: solids) {
            for (Part part: solid.getParts()) {
                switch (part.getType()){
                    case LINES:
                    case TRIANGLES: for (int i = 0; i < part.getCount(); i++){
                        int a = solid.getIndexBuffer().get(part.getStart() + 3*i);
                        int b = solid.getIndexBuffer().get(part.getStart() + 3*i +1);
                        int c = solid.getIndexBuffer().get(part.getStart() + 3*i + 2);
                        renderTriangle(solid.getVertexBuffer().get(a),
                                solid.getVertexBuffer().get(b),
                                solid.getVertexBuffer().get(c));

                    }
                    case LINES_STRIP:
                    case POINTS:
                }
            }

        }

    }


    private double minW = 0.01;
    private void renderTriangle(Vertex a, Vertex b,Vertex c){
            //Transformace MVP
        Mat4 f = finalMat();


        //Orezani
            if(a.getPoint().getX() > a.getPoint().getW() && b.getPoint().getX() > b.getPoint().getW() && c.getPoint().getX() > c.getPoint().getW()){
                return;
            }
            //...
            if(a.getPoint().getZ() < b.getPoint().getZ()){
                Vertex p = b;
                b=a;
                a=p;
            }
            if(b.getPoint().getZ() < c.getPoint().getZ()){
                Vertex p = c;
                b=c;
                c=p;
            }
            if(c.getPoint().getZ() >a.getPoint().getZ()){

            }
            if(a.getPoint().getZ()<0){
                return;
            }
            if(b.getPoint().getZ()<0){
                double t;
                Vertex ab = b.mul(1-t).add(a.mul(t));
                rt.rasterize(a,ab,ac);
            }
        }


    }


}
