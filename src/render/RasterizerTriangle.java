package render;

import model.Vertex;
import raster.TestVisibility;
import transforms.Col;

import java.util.function.Function;

public class RasterizerTriangle {

    private TestVisibility tv;
    private Function<Vertex,Col> shader;


    /*
    * public RasterizerTriangle(){
    * this.shader = shader
    * }
    *
    * neefektivní
    * */

    public RasterizerTriangle(){
        //shader=
    }

    public void setShader(Function<Vertex, Col> shader){
        this.shader = shader;
    }

    public void rasterize(Vertex a, Vertex b, Vertex c){
        a = a.dehomog();
        b = b.dehomog();
        c = c.dehomog();

        double xA = (a.getPoint().getX() + 1) * ((tv.getWidth()-1)/2);
        double yA = (-a.getPoint().getY() + 1) * (tv.getHeight()-1)/2;

        double xB = (b.getPoint().getX() + 1) * ((tv.getWidth()-1)/2);
        double yB = (-b.getPoint().getY() + 1) * (tv.getHeight()-1)/2;

        double xC = (c.getPoint().getX() + 1) * ((tv.getWidth()-1)/2);
        double yC = (-c.getPoint().getY() + 1) * (tv.getHeight()-1)/2;



        for (int y = (int) yA + 1; y < yB; y++) {

            //interpolace -minimum, deleno rozsahem

            double k = (y - yA)/(yB - yA);
            double x1, x2;
            double z1, z2;
            for (int x = x1; x < x2; x++)
            {
                double t = (x-x1)/(x2-x1);
                double z = z1*(1-t)

                tv.render(x, y, z, shader.apply());
            }

        }

    }




}
