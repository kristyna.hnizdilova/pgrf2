package render;

import model.Vertex;
import transforms.Col;

@FunctionalInterface
public interface Shader <V,C>{

    public C shade(V vertex);

}
